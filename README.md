<a href="http://www.youtube.com/watch?feature=player_embedded&v=EqLeRyjTlZw
" target="_blank"><img src="http://img.youtube.com/vi/EqLeRyjTlZw/0.jpg" 
alt="Pudliszek avoiding obstacles" width="240" height="180" border="10" /></a>
### ^ ^ ^
### Take a look how it works - open the link to YouTube video!
<br>
<br>
<br>
# ENG 
# Autonomic robot
The robot is able to discover and avoid obstacles. The algorithm of reacting to the obstacles uses an array of measured distances and relies on the median of them.

## Used spares
* STM32F4 Discovery
* servomechanizm SG90 + L293DNE
* Ultrasonic sensor HC-SR04
* Power supply: two batteries 18650 (2*3.7V) + stabilizer 7805

# POL 
# Robot autonomiczny
W ramach zajęć z PTM zrealizowaliśmy projekt robota autonomicznego omijającego przeszkody. Jako napęd posłużyły przerobione serwa modelarskie TowerPro SG90 (1.8kg/cm, 100rpm). Po usunięciu elektroniki serwomechanizmu oraz ogranicznika przekładni, oba silniki zostały podpięte do dwukanałowego mostka typu H (L293D). Silniki oraz logika robota są zasilanie z podwójnych ogniw 18650 (2 x 3.7V, realnie około 8.2V) przez stabilizator 5V 7805. Do rozpoznawania przeszkód posłużył nam popularny czujnik ultradźwiękowy HC-SR04, który umożliwiał mierzenie odległości od przeszkody w zakresie 2-200cm.

## Sprzęt
* STM32F4 Discovery
* Serwa SG90 + mostek L293DNE
* Ultradzwiękowy czujnik HC-SR04
* Zasilanie podwójnymi ogniwami 18650 (2*3.7V) + stabilizator 7805